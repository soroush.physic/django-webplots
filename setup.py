#!/usr/bin/env python

from pathlib import Path
from setuptools import setup

root = Path(__file__).parent or "."

with (root / "README.rst").open(encoding="utf-8") as f:
    long_description = f.read()

info = {
    "name":                 "django-webplots",
    "version":              "0.1.2",
    "author":               "Christoph Weinsheimer",
    "author_email":         "weinshec@holodeck2.de",
    "url":                  "https://gitlab.com/weinshec/django-webplots",
    "packages":             ["webplots"],
    "provides":             ["webplots"],
    "description":          "Using matplotlib in django applications",
    "long_description":     long_description,
    "keywords":             ["python", "matplotlib", "django", "plotting"],
    "install_requires":     ["django>=2.0", "matplotlib>=2.0", "Pillow"],
    "include_package_data": True,
    "zip_safe":             False,
}

if __name__ == "__main__":
    setup(**info)
