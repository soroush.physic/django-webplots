import shutil
import tempfile
from django.conf import settings
from django.test import TestCase


class VolatileMediaTestCase(TestCase):

    def setUp(self):
        self._temp_media = tempfile.mkdtemp()
        self._original_media_root = settings.MEDIA_ROOT
        self._original_file_storage = settings.DEFAULT_FILE_STORAGE
        settings.MEDIA_ROOT = self._temp_media
        settings.DEFAULT_FILE_STORAGE = \
            'django.core.files.storage.FileSystemStorage'

    def tearDown(self):
        shutil.rmtree(self._temp_media, ignore_errors=True)
        settings.MEDIA_ROOT = self._original_media_root
        settings.DEFAULT_FILE_STORAGE = self._original_file_storage
