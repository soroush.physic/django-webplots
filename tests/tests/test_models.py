from os.path import isfile
from unittest.mock import patch
from sampleapp.models import RenderMixinModel
from . import VolatileMediaTestCase


class RenderMixinTest(VolatileMediaTestCase):

    def test_saving_new_instance_creates_image(self):
        model = RenderMixinModel()
        self.assertEqual(model.render.name, "")

        model.save()
        self.assertNotEqual(model.render.name, "")
        self.assertTrue(isfile(model.render.path))

    def test_deleting_object_also_deletes_physical_file(self,):
        model = RenderMixinModel.objects.create()
        filepath = model.render.path

        self.assertTrue(isfile(filepath))
        model.delete()
        self.assertFalse(isfile(filepath))

    def test_update_save_removes_previous_file(self):
        model = RenderMixinModel.objects.create()

        filepath = model.render.path
        model.save()

        self.assertFalse(isfile(filepath))
        self.assertNotEqual(model.render.path, filepath)

    def test_can_retrieve_base64_encoded_render(self):
        with patch.object(RenderMixinModel, "draw", autospec=True) as mock:
            model = RenderMixinModel()
            b64 = model.render_to("base64", 1, additional="arguments")
            self.assertIsInstance(b64, str)
            mock.assert_called_once_with(model, 1, additional="arguments")

    def test_render_to_throws_exception_if_unkown_to(self):
        with patch.object(RenderMixinModel, "draw", autospec=True):
            model = RenderMixinModel()
            with self.assertRaises(NotImplementedError):
                model.render_to(to="foobar")

    def test_saving_new_instance_can_be_prevented_from_creating_an_image(self):
        model = RenderMixinModel()
        self.assertEqual(model.render.name, "")

        model.save(render=False)
        self.assertEqual(model.render.name, "")
